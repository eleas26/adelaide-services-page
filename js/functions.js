$( '.topheader .navbar-nav a' ).on( 'click', function () {
    $( '.topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
    $( this ).parent( 'li' ).addClass( 'active' );
});

$('#single-support-active li').on('click', function() {
    $('.single-support.active').removeClass('active');
    $(this).addClass('active');
});
$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 2000);
    }
});
new WOW().init();